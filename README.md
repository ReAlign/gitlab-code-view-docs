# gitlab-code-view-docs

## Release

### before 1.4.4

To be perfect...<br>
待完善...

### 1.4.4

#### Bugs

> Tree always show master's information<br>
> 目录树总是展示 master 的信息

When switching to another branch(not `master`), the tree always shows the master's information.<br>
当切换到另一个分支(非 `master`)时，目录树总是展示 master 的信息。

#### Optimization

Optimize the presentation of search results.<br>
优化搜索结果的展示。

##### Before

[![image_normal_2019-07-25_img-1564039120456-3136.png](https://i.loli.net/2019/07/25/5d39581517a1168699.png)](https://i.loli.net/2019/07/25/5d39581517a1168699.png)

##### After

[![image_normal_2019-07-25_img-1564039096996-4728.png](https://i.loli.net/2019/07/25/5d3958152ae2480707.png)](https://i.loli.net/2019/07/25/5d3958152ae2480707.png)